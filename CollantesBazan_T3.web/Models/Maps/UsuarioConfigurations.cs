using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CollantesBazan_T3.web.Models.Maps
{
    public class UsuarioConfigurations : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuario");
            builder.HasKey(o => o.Id);
//relacion mascota usuario
            builder.HasMany(o => o.mascotas).
                WithOne(o => o.usuario).HasForeignKey(o => o.IdUsuario);
        }
    }
}
