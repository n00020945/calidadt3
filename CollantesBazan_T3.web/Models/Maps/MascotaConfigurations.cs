using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CollantesBazan_T3.web.Models.Maps
{
    public class MascotaConfigurations: IEntityTypeConfiguration<Mascota>
    {
        public void Configure(EntityTypeBuilder<Mascota> builder)
        {
            builder.ToTable("Mascota");
            builder.HasKey(o => o.Id);
//relacion mascota historias clinicas
            builder.HasMany(o => o.historiaClinicas).WithOne(o => o.mascota).
                HasForeignKey(o => o.IdMascota);

            builder.HasOne(o => o.usuario).WithMany(o => o.mascotas).HasForeignKey(o => o.IdUsuario);

        }
    }
}
