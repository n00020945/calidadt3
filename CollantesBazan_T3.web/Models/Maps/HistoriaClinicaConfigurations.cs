using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CollantesBazan_T3.web.Models.Maps
{
    public class HistoriaClinicaConfigurations: IEntityTypeConfiguration<HistoriaClinica>
    {
        public void Configure(EntityTypeBuilder<HistoriaClinica> builder)
        {
            builder.ToTable("HistoriaClinica");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.mascota).WithMany(o => o.historiaClinicas).HasForeignKey(o => o.IdMascota);

        }
    }
}
