using System.Collections.Generic;

namespace CollantesBazan_T3.web.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Nombres { get; set; }
        
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }

        public List<Mascota> mascotas { get; set; }
    }
}
