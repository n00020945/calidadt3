using System;
using System.Collections.Generic;

namespace CollantesBazan_T3.web.Models
{
    public class Mascota
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaN { get; set; }
        public string Sexo { get; set; }
        public string Especie { get; set; }
        public string Raza { get; set; }
        public string Dimension { get; set; }
        public string DatosParticulares { get; set; }

        public int IdUsuario { get; set; }

        public Usuario usuario { get; set; }
        public List<HistoriaClinica> historiaClinicas { get; set; }
    }
}