using System;

namespace CollantesBazan_T3.web.Models
{
    public class HistoriaClinica
    {
        public int Id { get; set; }
        
        public string CodigoRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Descripcion { get; set; }

        public int IdMascota { get; set; }

        public Mascota mascota { get; set; }
    }
}