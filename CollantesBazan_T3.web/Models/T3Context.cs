using CollantesBazan_T3.web.Models.Maps;
using Microsoft.EntityFrameworkCore;

namespace CollantesBazan_T3.web.Models
{
    public class T3Context : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Mascota> Mascotas { get; set; }
        public DbSet<HistoriaClinica> HistoriaClinicas { get; set; }
        
        public T3Context(DbContextOptions<T3Context> options)
            : base(options) { }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UsuarioConfigurations()); 
            modelBuilder.ApplyConfiguration(new MascotaConfigurations()); 
            modelBuilder.ApplyConfiguration(new HistoriaClinicaConfigurations()); 

      
        }
        
        
    }
}