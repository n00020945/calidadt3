using System.Collections.Generic;
using CollantesBazan_T3.web.Models;
using CollantesBazan_T3.web.Repository;
using CollantesBazan_T3.web.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CollantesBazan_T3.web.Controllers
{
    [Authorize]
    public class VeterinariaController : Controller
    {
        
        private readonly IUsuarioRepository _usuario;
        private readonly IMascotaRepository _mascota;
        private readonly IHistoriaClinicaRepository _historiaClinica;
        private readonly ICookieAuthService _cookieAuthService;
        
        public VeterinariaController(IMascotaRepository _mascota,ICookieAuthService _cookieAuthService,IHistoriaClinicaRepository _historiaClinica,IUsuarioRepository _usuario)
        {
            this._usuario = _usuario;
            this._mascota = _mascota;
            this._historiaClinica = _historiaClinica;
            this._cookieAuthService = _cookieAuthService;
        }
        
      
        // GET
        public IActionResult Index()
        {
            Usuario user = LoggedUser();

            var ListaMascotas = _mascota.MisMascotas(user);
            ViewBag.usuario = user;
            return View(ListaMascotas);
        }
        
        public IActionResult HistoriasClinicas(int IdMascota)
        {
            Usuario user = LoggedUser();
            var MisHistoriasClinicas = _historiaClinica.MisHistoriasCLinicas(IdMascota);
            ViewBag.IdMascota = IdMascota;
             
            
            return View(MisHistoriasClinicas);
        }
        
        public IActionResult CrearMascota()
        {
            
            ViewBag.Sexo = Sexo();
            ViewBag.Raza = Raza();
            ViewBag.Especie = Especie();

           // var ListaHistoriasCLinicas = _historiaClinica.MisHistoriasCLinicas(IdMascota);
            
            return View();
        }
        public IActionResult GuardarMascota(Mascota mascota)
        {
            Usuario user = LoggedUser();
            _mascota.GuardarMascota(user,mascota);
     
            return RedirectToAction("Index");
        }
        public IActionResult CrearRegistro(int IdMascota)
        {
            ViewBag.IdMascota = IdMascota;
            return View();
        }
        public IActionResult GuardarRegistro(HistoriaClinica historiaClinica)
        {
            _historiaClinica.GuardarHistoriaClinica(historiaClinica);
            return RedirectToAction("Index");
        }
        
        
        
        
        private Usuario LoggedUser()
        {
        
            _cookieAuthService.SetHttpContext(HttpContext);
            var claim = _cookieAuthService.ObetenerClaim();
            var user = _usuario.UsuarioLogeado(claim);
            return user;
        }

        private List<string> Sexo()
        {
            List<string> sexos = new List<string>();
            sexos.Add("Macho");
            sexos.Add("Hembra");
            return sexos;
        }
        private List<string> Especie()
        {
            List<string> especie = new List<string>();
            especie.Add("Mamíferos");
            especie.Add("Aves");
            especie.Add("Ciempiés y milpiés");
            especie.Add("Reptiles");
            especie.Add("Peces");
            especie.Add("Caracoles, almejas y pulpos");
            especie.Add("Arañas y alacranes");
            especie.Add("Insectos");
          
            
            return especie;
        }
        private List<string> Raza()
        {
            List<string> raza = new List<string>();
            raza.Add("Perro");
            raza.Add("Gato");
            raza.Add("Araña");
            raza.Add("Conejo");
            raza.Add("Peces");
            raza.Add("Hampster");
            raza.Add("hormigas");
            raza.Add("serpiente");
            raza.Add("Loro");
            raza.Add("Perico");
            raza.Add("Otro");
          
            
            return raza;
        }
    }
}