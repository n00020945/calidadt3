using System.Collections.Generic;
using System.Linq;
using CollantesBazan_T3.web.Models;

namespace CollantesBazan_T3.web.Repository
{
    public interface IMascotaRepository
    {
        public List<Mascota> MisMascotas(Usuario usuario);
        public void GuardarMascota(Usuario usuario,Mascota mascota);
    }
    public class MascotaRepository : IMascotaRepository
    {
        private T3Context context;
        public MascotaRepository(T3Context context)
        {
            this.context = context;
        }

        public List<Mascota> MisMascotas(Usuario usuario)
        {
            var x= context.Mascotas.Where(o => o.IdUsuario == usuario.Id).ToList();
            return x;
        }

        public void GuardarMascota(Usuario usuario,Mascota mascota)
        {
            Mascota nuevaMascota = new Mascota();
            nuevaMascota = mascota;
            nuevaMascota.IdUsuario = usuario.Id;
            context.Mascotas.Add(nuevaMascota);
            context.SaveChanges();

        }
    }
}