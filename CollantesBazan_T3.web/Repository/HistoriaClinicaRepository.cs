using System;
using System.Collections.Generic;
using System.Linq;
using CollantesBazan_T3.web.Models;

namespace CollantesBazan_T3.web.Repository
{
    public interface IHistoriaClinicaRepository
    {
        public List<HistoriaClinica> MisHistoriasCLinicas(int IdMascota);
        public void GuardarHistoriaClinica(HistoriaClinica historiaClinica);
    }
    public class HistoriaClinicaRepository : IHistoriaClinicaRepository
    {
        private T3Context context;
        
        public HistoriaClinicaRepository(T3Context context)
        {
            this.context = context;
        }

        public List<HistoriaClinica> MisHistoriasCLinicas(int IdMascota)
        {
            return context.HistoriaClinicas.Where(o => o.IdMascota == IdMascota)
                .ToList();
        }

        public void GuardarHistoriaClinica(HistoriaClinica historiaClinica)
        {
            HistoriaClinica nuevaHistoriaClinica = new HistoriaClinica();
            nuevaHistoriaClinica = historiaClinica;
            var total =context.HistoriaClinicas.Count();
            nuevaHistoriaClinica.CodigoRegistro = "N"+DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + total;
            nuevaHistoriaClinica.FechaRegistro = DateTime.Now;
            context.HistoriaClinicas.Add(nuevaHistoriaClinica);
            context.SaveChanges();

        }
    }
}