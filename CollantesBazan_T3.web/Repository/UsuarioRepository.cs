using System.Linq;
using System.Security.Claims;
using CollantesBazan_T3.web.Models;

namespace CollantesBazan_T3.web.Repository
{
    public interface IUsuarioRepository
    {
        public Usuario EncontrarUsuario(string username, string password);
        public Usuario UsuarioLogeado(Claim claim);
    }
    public class UsuarioRepository : IUsuarioRepository
    {
        private T3Context context;

        public UsuarioRepository(T3Context context)
        {
            this.context = context;
        }

        public Usuario EncontrarUsuario(string username, string password)
        {
            var usuario =context.Usuarios.FirstOrDefault(o => o.Username == username && o.Password == password);
            return usuario;
        }

        public Usuario UsuarioLogeado(Claim claim)
        {
            var user = context.Usuarios.FirstOrDefault(o => o.Username == claim.Value);
            return user;
        }
    }
}