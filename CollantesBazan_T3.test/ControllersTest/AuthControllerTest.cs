using CollantesBazan_T3.web.Controllers;
using CollantesBazan_T3.web.Models;
using CollantesBazan_T3.web.Repository;
using CollantesBazan_T3.web.Service;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace CollantesBazan_T3.test
{
    public class AuthControllerTest
    {
        [Test]
        public void LoginRetornRedirecToAction()
        {                        
            var repositoryMock = new Mock<IUsuarioRepository>();
            repositoryMock.Setup(o => o.EncontrarUsuario("admin", "admin")).Returns(new Usuario { });

            var authMock = new Mock<ICookieAuthService>();
    

            var controller = new AuthController(repositoryMock.Object, authMock.Object);
            var result = controller.Login("admin", "admin");

            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }
        
        [Test]
        public void LoginRetornView()
        {                        
            var repositoryMock = new Mock<IUsuarioRepository>();
            

            var authMock = new Mock<ICookieAuthService>();
    

            var controller = new AuthController(repositoryMock.Object, authMock.Object);
            var result = controller.Login("admin", "admin");

            Assert.IsInstanceOf<ViewResult>(result);
        }
    }
}