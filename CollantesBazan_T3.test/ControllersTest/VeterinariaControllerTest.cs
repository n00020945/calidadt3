using System.Collections.Generic;
using CollantesBazan_T3.web.Controllers;
using CollantesBazan_T3.web.Models;
using CollantesBazan_T3.web.Repository;
using CollantesBazan_T3.web.Service;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace CollantesBazan_T3.test
{
    public class VeterinariaControllerTest
    {
        [Test]
        public void IndexReturnView()
        {  
            Usuario nuevo = new Usuario();
            var CookieAuthService = new Mock<ICookieAuthService>();
            var UsuarioRepositoryMock = new Mock<IUsuarioRepository>();
            UsuarioRepositoryMock.Setup(o => o.UsuarioLogeado(null)).Returns(nuevo);
            
          
            List<Mascota> misMascotas = new List<Mascota>();
            var MascotaRepositoryMock = new Mock<IMascotaRepository>();
            MascotaRepositoryMock.Setup(o => o.MisMascotas(nuevo)).Returns(misMascotas);
            var controller = new VeterinariaController(MascotaRepositoryMock.Object,CookieAuthService.Object,null,UsuarioRepositoryMock.Object);
            var view =controller.Index();
            Assert.IsInstanceOf<ViewResult>(view);
        } 
        
        [Test]
        public void HistoriasClinicasReturnView()
        {  
            Usuario nuevo = new Usuario();
            var CookieAuthService = new Mock<ICookieAuthService>();
            var UsuarioRepositoryMock = new Mock<IUsuarioRepository>();
            UsuarioRepositoryMock.Setup(o => o.UsuarioLogeado(null)).Returns(nuevo);
            
          
            List<HistoriaClinica> historiaClinicas = new List<HistoriaClinica>();
            var HistoriaClinicaRepositoryMock = new Mock<IHistoriaClinicaRepository>();
            HistoriaClinicaRepositoryMock.Setup(o => o.MisHistoriasCLinicas(2)).Returns(historiaClinicas);
            var controller = new VeterinariaController(null,CookieAuthService.Object,HistoriaClinicaRepositoryMock.Object,UsuarioRepositoryMock.Object);
            var view =controller.HistoriasClinicas(2);
            Assert.IsInstanceOf<ViewResult>(view);
        } 
        
        [Test]
        public void CrearMascota()
        {  
             var controller = new VeterinariaController(null,null,null,null);
            var view =controller.CrearMascota();
            Assert.IsInstanceOf<ViewResult>(view);
        } 
        
        [Test]
        public void GuardarMascotaReturnRedirecToView()
        {  
            Usuario nuevo = new Usuario();
            var CookieAuthService = new Mock<ICookieAuthService>();
            var UsuarioRepositoryMock = new Mock<IUsuarioRepository>();
            UsuarioRepositoryMock.Setup(o => o.UsuarioLogeado(null)).Returns(nuevo);
            
          
            List<Mascota> misMascotas = new List<Mascota>();
            var MascotaRepositoryMock = new Mock<IMascotaRepository>();
            MascotaRepositoryMock.Setup(o => o.GuardarMascota(new Usuario(), new Mascota()));
            var controller = new VeterinariaController(MascotaRepositoryMock.Object,CookieAuthService.Object,null,UsuarioRepositoryMock.Object);
            var view =controller.GuardarMascota(new Mascota());
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        } 
        
        [Test]
        public void CrearRegistroReturnView()
        {  
            var controller = new VeterinariaController(null,null,null,null);
            var view =controller.CrearRegistro(2);
            Assert.IsInstanceOf<ViewResult>(view);
        } 
        
        [Test]
        public void GuardarRegistroReturnRedirecToView()
        {  
            Usuario nuevo = new Usuario();
            var CookieAuthService = new Mock<ICookieAuthService>();
            var UsuarioRepositoryMock = new Mock<IUsuarioRepository>();
            UsuarioRepositoryMock.Setup(o => o.UsuarioLogeado(null)).Returns(nuevo);
            
          
            List<Mascota> misMascotas = new List<Mascota>();
            var HistoriaClinicaRepositoryMock = new Mock<IHistoriaClinicaRepository>();
            HistoriaClinicaRepositoryMock.Setup(o => o.GuardarHistoriaClinica(new HistoriaClinica()));
            var controller = new VeterinariaController(null,CookieAuthService.Object,HistoriaClinicaRepositoryMock.Object,UsuarioRepositoryMock.Object);
            var view =controller.GuardarRegistro(new HistoriaClinica());
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        } 
    }
}